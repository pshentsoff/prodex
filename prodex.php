<?php

/*
Plugin Name: Prodex
Plugin URI:
Description: Prodex: Типы записей, метабоксы и мигратор данных БД
Author: Vadim Pshentsov <pshentsoff@gmail.com>
Version: 0.4.1-dev
Author URI: http://pshentsoff.ru
*/

/**
 * @file        prodex.php
 * @description
 *
 * PHP Version  5.3.13
 *
 * @package 
 * @category
 * @plugin URI
 * @copyright   2013, Vadim Pshentsov. All Rights Reserved.
 * @license     http://www.apache.org/licenses/LICENSE-2.0 Apache License, Version 2.0
 * @author      Vadim Pshentsov <pshentsoff@gmail.com> 
 * @link        http://pshentsoff.ru Author's homepage
 * @link        http://blog.pshentsoff.ru Author's blog
 *
 * @created     06.11.13
 */

define('PRODEX_PREFIX', 'prodex');
define('PRODEX_FIELD_PREFIX', PRODEX_PREFIX.'_');
define('PRODEX_ID_PREFIX', PRODEX_PREFIX.'-');
define('PRODEX_TYPE_CLIENT', 'client');
define('PRODEX_SERVICE_TYPE_DATA','_service_types_data');
define('PRODEX_TAXONOMY_CLIENTS', 'clients');

if(is_admin()) {
    add_action( 'admin_menu', 'prodex_admin_menu');
    function prodex_admin_menu() {
        add_management_page(__('Миграция данных из старой БД Prodex'),__('Prodex: миграция'),'manage_options','prodex-data-migration','prodex_data_migration_page');
    }
}

function prodex_data_migration_page() {

    global $wpdb, $wp_version;

//    if(!isset($wp_version)) require(ABSPATH . WPINC . '/version.php');
    $version = explode('.', $wp_version);

    $msg = '';
    $connected = false;

    $passwd =  isset($_POST['source_password']) ? $_POST['source_password'] : '';
    if(isset($_POST['source_dbhost']) && isset($_POST['source_dbname']) && isset($_POST['source_user'])) {
        $sourcedb = new wpdb($_POST['source_user'], $passwd, $_POST['source_dbname'],$_POST['source_dbhost']);
        $connected = isset($sourcedb) && $sourcedb->ready;
    }


    if(isset($_POST['action_check_connect']) && $connected) {
        $msg .= __('Соединение успешно установлено.');
    }

    $sql_clients = 'SELECT id, name, logo, url1, url2, url3, description, type, result, date, manager, seospec, address ';
    $sql_clients .= ' FROM clients';
    $sql_clients .= ' ORDER BY id';

    $sql_clients2themes = 'SELECT  client_to_theme.themeID AS id, theme.name AS name';
    $sql_clients2themes .= ' FROM client_to_theme, theme';
    $sql_clients2themes .= ' WHERE clientID = %d';
    $sql_clients2themes .= ' AND theme.id = themeID';
    $sql_clients2themes .= ' ORDER BY id';

    if(!empty($msg)) $msg .= "<br />\n";
    if(isset($_POST['action_check_tables_structure']) && $connected) {

        $clients = $sourcedb->get_results($sql_clients, OBJECT_K);

        //@todo проверить theme, client_to_theme

        if(isset($clients)) {
            $clients_count = count($clients);
            $msg .= sprintf(__('Данные таблицы клиентов успешно получены. Найдено %d записей.'), $clients_count);

//            echo '<pre>'.print_r($clients['18'], true).'</pre>';
        } else {
            $msg .= __('Не удалось получить данные таблицы клиентов.');
        }

    }


    if(isset($_POST['action_migrate']) && $connected) {

        $clients = $sourcedb->get_results($sql_clients, OBJECT_K);

        if(isset($clients)) {

            $clients_count = count($clients);

            if(!empty($msg)) $msg .= "<br />\n";
            $msg .= sprintf(__('Данные таблицы клиентов успешно получены. Найдено %d записей.'), $clients_count);

//            echo '<pre>'.print_r($clients['18'], true).'</pre>';
/*            $user_id = get_current_user_id();

            $defaults = array('post_status' => 'draft', 'post_type' => PRODEX_TYPE_CLIENT, 'post_author' => $user_id,
                'ping_status' => get_option('default_ping_status'), 'post_parent' => 0,
                'menu_order' => 0, 'to_ping' =>  '', 'pinged' => '', 'post_password' => '',
                'guid' => '', 'post_content_filtered' => '', 'post_excerpt' => '', 'import_id' => 0,
                'post_content' => '', 'post_title' => '');*/

            $post = array(
                'post_status' => 'publish',
                'post_type' => PRODEX_TYPE_CLIENT,
            );

            foreach ($clients as $id => $client) {

                $clients[$id]->type = unserialize($client->type);

                // Сохраняем новый пост типа клиент
                $post['post_title'] = $client->name;
                $post['post_content'] = $client->description;
                $postID = wp_insert_post($post);

                // Сохраняем метабокс 1
                $service_type['seo_optimization'] = (isset($clients[$id]->type[0]) && ($clients[$id]->type[0] == 'on')) ? 1 : 0;
                $service_type['context_advertising'] = (isset($clients[$id]->type[1]) && ($clients[$id]->type[1] == 'on')) ? 1 : 0;
                $service_type['banner_advertising'] = (isset($clients[$id]->type[2]) && ($clients[$id]->type[2] == 'on')) ? 1 : 0;
                $service_type['site_development'] = (isset($clients[$id]->type[3]) && ($clients[$id]->type[3] == 'on')) ? 1 : 0;
                $service_type['banner_development'] = (isset($clients[$id]->type[4]) && ($clients[$id]->type[4] == 'on')) ? 1 : 0;
                $service_type['price_aggregator_advertising'] = (isset($clients[$id]->type[5]) && ($clients[$id]->type[5] == 'on')) ? 1 : 0;
                $service_type['social_networks_promotion'] = (isset($clients[$id]->type[6]) && ($clients[$id]->type[6] == 'on')) ? 1 : 0;
                update_post_meta($postID, PRODEX_SERVICE_TYPE_DATA, $service_type);

                // Сохраняем метабокс 2
                update_post_meta($postID, '_additional_url1', $client->url1);
                update_post_meta($postID, '_additional_url2', $client->url2);
                update_post_meta($postID, '_additional_url3', $client->url3);
                //result, date, manager, seospec, address
                update_post_meta($postID, '_additional_results', $client->result);
                update_post_meta($postID, '_additional_date', $client->date);
                update_post_meta($postID, '_additional_manager', $client->manager);
                update_post_meta($postID, '_additional_seo_specialist', $client->seospec);
                update_post_meta($postID, '_additional_address', $client->address);

                // Добавим связанные категориии, если таких еще нет
                $sql = sprintf($sql_clients2themes, $id);
                if($client_themes = $sourcedb->get_results($sql, OBJECT_K)) {

                    unset($catIDs);
                    foreach($client_themes as $themeID => $theme) {
                        if(!term_exists($theme->name, PRODEX_TAXONOMY_CLIENTS)) {
                            wp_insert_term($theme->name, PRODEX_TAXONOMY_CLIENTS);
                        }
                        $catIDs[] = $theme->name;
                    }
                    wp_set_object_terms($postID, $catIDs, PRODEX_TAXONOMY_CLIENTS);
                }

                // Теперь с картинками
                // загружаем времменную картинку
                if(!empty($client->logo)) {

                    $tmp = download_url( $client->logo );

                    if(is_wp_error( $tmp )) {
                        if(!empty($msg)) $msg .= "<br />\n";
                        $tmp = sprintf(__('Ошибка при проверке доступности файла изображения "%s". Патчим.'),$client->logo);
//                        echo "<pre>$tmp</pre>";
                        $msg .= $tmp;
                        $client->logo = 'http://www.prodex.net.ua'.$client->logo;

                        $tmp = download_url( $client->logo );
                        if(is_wp_error( $tmp )) {
                            if(!empty($msg)) $msg .= "<br />\n";
                            $tmp = sprintf(__('Ошибка при проверке доступности файла изображения "%s". Уходим.'),$client->logo);
//                            echo "<pre>$tmp</pre>";
                            $msg .= $tmp;
                            continue;
                        }
                    }

//                    echo '<pre>['.$client->logo.'] для ['.$client->name.']</pre>';


                    $desc = $client->name;
                    preg_match('/[^\?]+\.(jpg|JPG|jpe|JPE|jpeg|JPEG|gif|GIF|png|PNG)/', $client->logo, $matches);
                    $file_array['name'] = basename($matches[0]);
                    $file_array['tmp_name'] = $tmp;

                    // Если ошибка при загрузке временного - удаляем временный
                    if ( is_wp_error( $tmp ) ) {
                        @unlink($file_array['tmp_name']);
                        $file_array['tmp_name'] = '';
                        if(!empty($msg)) $msg .= "<br />\n";
                        $msg .= sprintf(__('Ошибка при загрузке файла изображения "%s" указаного как логотип клиента "%s"'),$client->logo, $client->name);
                    } else {

                        // сохранение как постоянного с занесением в БД
                        $imgID = media_handle_sideload( $file_array, $postID, $desc );

                        // Если ошибка при сохранении постоянного фала - удаляем
                        if ( is_wp_error($imgID) ) {
                            @unlink($file_array['tmp_name']);
                            if(!empty($msg)) $msg .= "<br />\n";
                            $msg .= sprintf(__('Ошибка при сохранении изображения "%s" указаного как логотип клиента "%s"'), $client->logo, $client->name);
                        } else {
                            // Иначе - сохраняем как миниатюру поста а специальном служебном поле _thumbnail_id
                            update_post_meta($postID, '_thumbnail_id', $imgID);
                        }
                    }
                }
            }

            if(!empty($msg)) $msg .= "<br />\n";
            $msg .= sprintf(__('Клиенты перенесены успешно. Перенесено %d клиентов.'), count($clients));
        } else {
            if(!empty($msg)) $msg .= "<br />\n";
            $msg .= __('Не удалось получить данные таблицы клиентов.');
        }

    }


    if(!empty($msg)) $msg .= "<br />\n";
    if(isset($_POST['action_migrate_categories']) && $connected) {

        $sql_themes = 'SELECT id, name, pos FROM theme ORDER BY pos, name';

        $themes = $sourcedb->get_results($sql_themes, OBJECT_K);

        if(isset($themes)) {
            foreach($client_themes as $themeID => $theme) {
                if(!term_exists($theme->name, PRODEX_TAXONOMY_CLIENTS)) {
                    wp_insert_term($theme->name, PRODEX_TAXONOMY_CLIENTS);
                }
            }
            $msg .= sprintf(__('Темы перенесены успешно. В рубриках добавлены %d новых категории.'), count($themes));
        }
    }

?>
    <div class="wrap">
        <div id="icon-options-general" class="icon32"><br /></div>
        <h2><?php _e('Миграция данных из старой БД Prodex'); ?></h2>

        <?php if(!empty($msg)) { ?>
        <div id="message" class="updated"><p>
            <?php echo $msg; ?>
        </p></div>
        <?php } ?>

        <form method="post" action="">
            <?php wp_nonce_field('prodex_data_migration_action','prodex_data_migration_nonce'); ?>
            <p>
                <p>
                <h3><?php _e('БД - источник (прежняя БД Prodex)'); ?></h3>
                <label for="source-dbhost"><?php _e('Сервер БД'); ?>:&nbsp;<input id="source-dbhost" type="text" name="source_dbhost" value="<?php echo (isset($_POST['source_dbhost'])?$_POST['source_dbhost']:DB_HOST); ?>"/></label><br />
                <label for="source-dbname"><?php _e('База данных'); ?>:&nbsp;<input id="source-dbname" type="text" name="source_dbname" value="<?php echo (isset($_POST['source_dbname'])?$_POST['source_dbname']:'new_prodex_db'); ?>"/></label><br />
                <label for="source-user"><?php _e('Пользователь'); ?>:&nbsp;<input id="source-user" type="text" name="source_user" value="<?php echo (isset($_POST['source_user'])?$_POST['source_user']:DB_USER); ?>"/></label><br />
                <label for="source-password"><?php _e('Пароль'); ?>:&nbsp;<input id="source-password" type="password" name="source_password" value="<?php echo (isset($_POST['source_password'])?$_POST['source_password']:DB_PASSWORD); ?>"/></label><br />
                </p>
                <input type="submit" name="action_check_connect" value="<?php _e('Проверить соединение'); ?>" />
            </p>
            <p>
                <input type="submit" name="action_check_tables_structure" value="<?php _e('Проверить стурктуру таблиц'); ?>" />
            </p>
            <p>
                <input type="submit" name="action_migrate_categories" value="<?php _e('Перенести категории'); ?>" />
            </p>
            <p>
                <input type="submit" name="action_migrate" value="<?php _e('Перенести данные по клиентам'); ?>" />
            </p>
        </form>
    </div>
    <?php
}

// 1.	Создаем кастомный тип записей client
add_action( 'init', 'prodex_create_post_type' );
function prodex_create_post_type() {
    register_post_type( PRODEX_TYPE_CLIENT,
        array(
            'labels' => array(
                'name' => __( 'Клиент' ),
                'singular_name' => __( 'Клиент' ),
                'search_items' => __('Поиск клиентов'),
                'edit_item' => __('Редактировать клиента'),
                'update_item' => __('Обновить клиента'),
                'add_new_item' => __('Добавить нового клиента'),
                'new_item_name' => __('Имя нового клиента'),
                'add_new' => __('Добавить клиента'),
                'not_found' => __('Ничего не найдено'),
                'not_found_in_trash' => __('В корзине клиентов не найдено'),
                'menu_name' => __('Клиенты'),
            ),
            'public' => true,
            'has_archive' => true,
            'supports' => array(
                'title',
                'editor',
                'author',
                'trackbacks',
                'thumbnail',
                'page-attributes',
                'post-formats',
                'custom-fields'
            ),
            'taxonomies' => array(PRODEX_TAXONOMY_CLIENTS),
        ));

    prodex_build_taxonomies();
}

/**
 * Создаем новую таксономию 'clients' и регистрируем ее к новому типу поста 'client'
 */
function prodex_build_taxonomies() {

    $labels = array(
        'name' => __('Группы клиентов'),
        'singular_name' => __('Группа'),
        'search_items' => __('Поиск групп клиентов'),
        'edit_item' => __('Редактировать группу'),
        'update_item' => __('Обновить группу'),
        'add_new_item' => __('Добавить новую группу'),
        'new_item_name' => __('Имя новой группы'),
        'menu_name' => __('Группы клиентов'),
    );

    $params = array(
        'labels' => $labels,
//        'hierarchical' => true,
    );

    register_taxonomy(PRODEX_TAXONOMY_CLIENTS, PRODEX_TYPE_CLIENT, $params);

}

add_action('save_post', 'prodex_save_client_meta', 10, 2);
function prodex_save_client_meta($postID, $post) {

    // Не сохраняем метабоксы для ревизии
    if (wp_is_post_revision($postID)) return;
    // Не сохраняем метабоксы при автосохранении
    if ((defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
        ||(isset($_POST['action']) && $_POST['action'] == 'autosave'))
        return;

    if(isset($_POST[PRODEX_FIELD_PREFIX.'service_type_hidden_field'])) {
        prodex_save_service_type($postID, $post);
    }

    if(isset($_POST[PRODEX_FIELD_PREFIX.'additional_hidden_field'])) {
        prodex_save_additional($postID, $post);
    }
}

function prodex_save_service_type($postID, $post) {
    // Проверка
    check_admin_referer(PRODEX_FIELD_PREFIX."service_types_action", PRODEX_FIELD_PREFIX."service_types_nonce");

    $service_types = array(
        'seo_optimization' => (($_POST[PRODEX_FIELD_PREFIX.'seo_optimization'] == 'on') ? 1 : 0),
        'context_advertising' => (($_POST[PRODEX_FIELD_PREFIX.'context_advertising'] == 'on') ? 1 : 0),
        'banner_advertising' => (($_POST[PRODEX_FIELD_PREFIX.'banner_advertising'] == 'on') ? 1 : 0),
        'site_development' => (($_POST[PRODEX_FIELD_PREFIX.'development'] == 'on') ? 1 : 0),
        'banner_development' => (($_POST[PRODEX_FIELD_PREFIX.'banner_development'] == 'on') ? 1 : 0),
        'price_aggregator_advertising' => (($_POST[PRODEX_FIELD_PREFIX.'price_aggregator_advertising'] == 'on') ? 1 : 0),
        'social_networks_promotion' => (($_POST[PRODEX_FIELD_PREFIX.'social_networks_promotion'] == 'on') ? 1 : 0),
    );
    update_post_meta($postID, PRODEX_SERVICE_TYPE_DATA, $service_types);
}

function prodex_save_additional($postID, $post) {
    // Проверка
    check_admin_referer(PRODEX_FIELD_PREFIX."additional_action", PRODEX_FIELD_PREFIX."additional_nonce");

    $data = array();
    $data['url1'] = sanitize_text_field($_POST[PRODEX_FIELD_PREFIX.'url1']);
    $data['url2'] = sanitize_text_field($_POST[PRODEX_FIELD_PREFIX.'url2']);
    $data['url3'] = sanitize_text_field($_POST[PRODEX_FIELD_PREFIX.'url3']);
    $data['date'] = sanitize_text_field($_POST[PRODEX_FIELD_PREFIX.'date']);
    $data['results'] = sanitize_text_field($_POST[PRODEX_FIELD_PREFIX.'results']);
    $data['address'] = sanitize_text_field($_POST[PRODEX_FIELD_PREFIX.'address']);
    $data['manager'] = sanitize_text_field($_POST[PRODEX_FIELD_PREFIX.'manager']);
    $data['seo_specialist'] = sanitize_text_field($_POST[PRODEX_FIELD_PREFIX.'seo_specialist']);

    foreach ($data as $field => $value) {
        update_post_meta($postID, '_additional_'.$field, $value);
    }


}

add_action('add_meta_boxes', 'prodex_metaboxes_init');
function prodex_metaboxes_init() {
    //3.	Создаем метабокс, называем его “Тип услуг”
    add_meta_box('service_types', __('Типы услуг'), 'prodex_service_types_showup', 'client', 'side', 'high');
//    4. Создаем новый метабокс, называем его “Дополнительно”
    add_meta_box('additional', __('Дополнительно'), 'prodex_additional_showup', 'client', 'advanced', 'high');
}

// 3.	Создаем метабокс, называем его “Тип услуг”, в нем будут находиться чекбоксы :
function prodex_service_types_showup($post, $box) {

    $data = get_post_meta($post->ID, '_service_types_data', true);

    // Защита
    wp_nonce_field(PRODEX_FIELD_PREFIX."service_types_action", PRODEX_FIELD_PREFIX."service_types_nonce");
    ?>
    <input type="hidden" name="<?php echo PRODEX_FIELD_PREFIX ?>service_type_hidden_field" value="1"/>
    <label for="<?php echo PRODEX_ID_PREFIX ?>seo-optimization"><input id="<?php echo PRODEX_ID_PREFIX ?>seo-optimization" name="<?php echo PRODEX_FIELD_PREFIX ?>seo_optimization" type="checkbox" <?php echo ($data['seo_optimization'] ? 'checked="checked"' : '');?> />&nbsp;<?php _e('Поисковая оптимизация'); ?></label><br />
    <label for="<?php echo PRODEX_ID_PREFIX ?>context-advertising"><input id="<?php echo PRODEX_ID_PREFIX ?>context-advertising" name="<?php echo PRODEX_FIELD_PREFIX ?>context_advertising" type="checkbox" <?php echo ($data['context_advertising'] ? 'checked="checked"' : '');?> />&nbsp;<?php _e('Контекстная реклама'); ?></label><br />
    <label for="<?php echo PRODEX_ID_PREFIX ?>banner-advertising"><input id="<?php echo PRODEX_ID_PREFIX ?>banner-advertising" name="<?php echo PRODEX_FIELD_PREFIX ?>banner_advertising" type="checkbox" <?php echo ($data['banner_advertising'] ? 'checked="checked"' : '');?> />&nbsp;<?php _e('Банерная реклама'); ?></label><br />
    <label for="<?php echo PRODEX_ID_PREFIX ?>site-development"><input id="<?php echo PRODEX_ID_PREFIX ?>site-development" name="<?php echo PRODEX_FIELD_PREFIX ?>site_development" type="checkbox" <?php echo ($data['site_development'] ? 'checked="checked"' : '');?> />&nbsp;<?php _e('Разработка сайта'); ?></label><br />
    <label for="<?php echo PRODEX_ID_PREFIX ?>banner-development"><input id="<?php echo PRODEX_ID_PREFIX ?>banner-development" name="<?php echo PRODEX_FIELD_PREFIX ?>banner_development" type="checkbox" <?php echo ($data['banner_development'] ? 'checked="checked"' : '');?> />&nbsp;<?php _e('Разработка банера'); ?></label><br />
    <label for="<?php echo PRODEX_ID_PREFIX ?>price-aggregator-advertising"><input id="<?php echo PRODEX_ID_PREFIX ?>price-aggregator-advertising" name="<?php echo PRODEX_FIELD_PREFIX ?>price_aggregator_advertising" type="checkbox" <?php echo ($data['price_aggregator_advertising'] ? 'checked="checked"' : '');?> />&nbsp;<?php _e('Реклама в прайс-агрегаторах'); ?></label><br />
    <label for="<?php echo PRODEX_ID_PREFIX ?>social-networks-promotion"><input id="<?php echo PRODEX_ID_PREFIX ?>social-networks-promotion" name="<?php echo PRODEX_FIELD_PREFIX ?>social_networks_promotion" type="checkbox" <?php echo ($data['social_networks_promotion'] ? 'checked="checked"' : '');?> />&nbsp;<?php _e('Продвижение в социальных сетях'); ?></label><br />
    <?php
//    echo '<pre>$post = '.print_r($post, true).'</pre>';
//    echo '<pre>$box = '.print_r($box, true).'</pre>';
//    echo '<pre>$data = '.print_r($data, true).'</pre>';
}

// 4. Создаем новый метабокс, называем его “Дополнительно”, в нем будут текстовые поля
function prodex_additional_showup($post, $box) {

    $data = array();
    $data['url1'] = get_post_meta($post->ID, '_additional_url1', true);
    $data['url2'] = get_post_meta($post->ID, '_additional_url2', true);
    $data['url3'] = get_post_meta($post->ID, '_additional_url3', true);
    $data['date'] = get_post_meta($post->ID, '_additional_date', true);
    $data['results'] = get_post_meta($post->ID, '_additional_results', true);
    $data['address'] = get_post_meta($post->ID, '_additional_address', true);
    $data['manager'] = get_post_meta($post->ID, '_additional_manager', true);
    $data['seo_specialist'] = get_post_meta($post->ID, '_additional_seo_specialist', true);

    // Защита
    wp_nonce_field(PRODEX_FIELD_PREFIX.'additional_action', PRODEX_FIELD_PREFIX.'additional_nonce');
    ?>
    <input type="hidden" name="<?php echo PRODEX_FIELD_PREFIX ?>additional_hidden_field" value="1"/>
    <label for="<?php echo PRODEX_ID_PREFIX ?>url1"><?php _e('Url1'); ?>:&nbsp;</label><input style="width: 400px;" value="<?php echo $data['url1']; ?>" id="<?php echo PRODEX_ID_PREFIX ?>url1" name="<?php echo PRODEX_FIELD_PREFIX ?>url1" type="text" /><br />
    <label for="<?php echo PRODEX_ID_PREFIX ?>url2"><?php _e('Url2'); ?>:&nbsp;</label><input style="width: 400px;" value="<?php echo $data['url2']; ?>" id="<?php echo PRODEX_ID_PREFIX ?>url2" name="<?php echo PRODEX_FIELD_PREFIX ?>url2" type="text" /><br />
    <label for="<?php echo PRODEX_ID_PREFIX ?>url3"><?php _e('Url3'); ?>:&nbsp;</label><input style="width: 400px;" value="<?php echo $data['url3']; ?>" id="<?php echo PRODEX_ID_PREFIX ?>url3" name="<?php echo PRODEX_FIELD_PREFIX ?>url3" type="text" /><br />
    <label for="<?php echo PRODEX_ID_PREFIX ?>date"><?php _e('Дата'); ?>:&nbsp;</label><input style="width: 400px;" value="<?php echo $data['date']; ?>" id="<?php echo PRODEX_ID_PREFIX ?>date" name="<?php echo PRODEX_FIELD_PREFIX ?>date" type="text" /><br />
    <label for="<?php echo PRODEX_ID_PREFIX ?>results"><?php _e('Результаты'); ?>:&nbsp;</label><textarea style="width: 400px; height:100px;" id="<?php echo PRODEX_ID_PREFIX ?>results" name="<?php echo PRODEX_FIELD_PREFIX ?>results" type="text" ><?php echo $data['results']; ?></textarea><br />
    <label for="<?php echo PRODEX_ID_PREFIX ?>address"><?php _e('Адрес'); ?>:&nbsp;</label><input style="width: 400px;" value="<?php echo $data['address']; ?>" id="<?php echo PRODEX_ID_PREFIX ?>address" name="<?php echo PRODEX_FIELD_PREFIX ?>address" type="text" /><br />
    <label for="<?php echo PRODEX_ID_PREFIX ?>manager"><?php _e('Менеджер'); ?>:&nbsp;</label><input style="width: 400px;" value="<?php echo $data['manager']; ?>" id="<?php echo PRODEX_ID_PREFIX ?>manager" name="<?php echo PRODEX_FIELD_PREFIX ?>manager" type="text" /><br />
    <label for="<?php echo PRODEX_ID_PREFIX ?>seo-specialist"><?php _e('Seo спец.'); ?>:&nbsp;</label><input style="width: 400px;" value="<?php echo $data['seo_specialist']; ?>" id="<?php echo PRODEX_ID_PREFIX ?>seo-specialist" name="<?php echo PRODEX_FIELD_PREFIX ?>seo_specialist" type="text" /><br />
<?php
//    echo '<pre>$post = '.print_r($post, true).'</pre>';
//    echo '<pre>$box = '.print_r($box, true).'</pre>';
//    echo '<pre>$data = '.print_r($data, true).'</pre>';
//    echo '<pre>$_POST='.print_r($_POST, true).'</pre>';

}

